import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import Shellexec 1.0
//import Indicator 1.0

Page {

    header: PageHeader {
        id: header
        title: "uStats"

        Shellexec {
          id: launcherhead
        }

        trailingActionBar.actions: [
            Action {
                iconName: "info"
                text: i18n.tr("About")
                onTriggered: pageStack.push(Qt.resolvedUrl("AboutPage.qml"))
            },
            Action {
                iconName: "reload"
                text: i18n.tr("Reload data")
                onTriggered: {
                  outputField.text = ""
                  outputField.text = launcherhead.launch("vnstat");
                }
              },
            Action {
                iconName: "reset"
                text: i18n.tr("Clear output")
                onTriggered: {
                  outputField.text = "console output should appear here"
                  command_input_field.text = ""
                  }
                }
        ]
    }
    Shellexec {
      id: launcher
    }
    // function execute(commandstring) {
    //     return launcher.launch("sh -c \"" + commandstring + " < /dev/null \"")
    // }

    Flickable {
        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        clip: true
        contentHeight: contentColumn.height + units.gu(4)

        ColumnLayout {
            id: contentColumn
            anchors {
                left: parent.left
                top: parent.top
                right: parent.right
                margins: units.gu(2)
            }
            spacing: units.gu(1)

            RowLayout {
              id: first_row_layout

              Button {
                id: vmstatButton
                height: units.gu(5)
                width: units.gu(20)
                anchors {
                    topMargin: units.gu(2)
                    leftMargin: units.gu(1)
                    horizontalCenter: parent.horizontalleft
                }
                text: "vmstat --stats"
                onTriggered: {
                  outputField.text = "",
                  outputField.text = launcher.launch("vmstat --stats"),
                  console.log("command: " + "vmstat --stats" + "\nexecute result: \n" + launcher.launch("vmstat --stats"))
                }
              }

              Button {
                id: vnstatButton
                height: units.gu(5)
                width: units.gu(20)
                anchors {
                    topMargin: units.gu(2)
                    leftMargin: units.gu(2)
                    horizontalCenter: parent.horizontalright
                }
                text: "vnstat"
                onTriggered: {
                  outputField.text = "",
                  outputField.text = launcher.launch("vnstat"),
                  console.log("command: " + "vnstat" + "\nexecute result: \n" + launcher.launch("vnstat"))
                }
              }

              Button {
                id: vnstatDaysButton
                height: units.gu(5)
                width: units.gu(20)
                anchors {
                    topMargin: units.gu(2)
                    leftMargin: units.gu(2)
                    horizontalCenter: parent.horizontalright
                }
                text: "vnstat --days"
                onTriggered: {
                  outputField.text = "",
                  outputField.text = launcher.launch("vnstat --days"),
                  console.log("command: " + "vnstat --days --short" + "\nexecute result: \n" + launcher.launch("vnstat --days --short"))
                }
              }
            }

            Label {
                id: descriptiontext
                visible: true
                text: i18n.tr("Execute a custom command:")
                Layout.fillWidth: true
            }

            RowLayout {
              id: second_row_layout



              TextField {
                  id: command_input_field

                  width: parent.width - units.gu(2)
                  anchors {
                      topMargin: units.gu(2)
                      horizontalCenter: parent.horizontalleft
                  }
                  text: ""
                  placeholderText: "enter command here"
                  inputMethodHints: {
                    Qt.ImhLowercaseOnly,
                    Qt.ImhNoPredictiveText
                  }
                  onAccepted: {
                     outputField.text = "command: " + text
                  }
              }
              Button {
                id: execButton
                height: units.gu(5)
                width: units.gu(15)
                anchors {
                    topMargin: units.gu(2)
                    leftMargin: units.gu(1)
                    horizontalCenter: parent.horizontalleft
                }
                text: i18n.tr("execute")
                onTriggered: {
                  outputField.text = "",
                  outputField.text = launcher.launch(command_input_field.text),
                  console.log("execute result: \n" + launcher.launch(command_input_field.text))
                }
              }
            }

            Rectangle {
              id: resultArea
              anchors {
                  left: parent.left
                  top: second_row_layout.bottom
                  right: parent.right
                  margins: units.gu(1)
              }

              Text {
                id: outputField
                width: parent.width
                text: i18n.tr("command output should appear here")
                wrapMode: Text.WordWrap
              }

           }
        }
    }
}
