#include <QtQml>
#include <QtQml/QQmlContext>

#include "plugin.h"
#include "shellexec.h"

void ShellexecPlugin::registerTypes(const char *uri) {
    //@uri Shellexec
    //qmlRegisterSingletonType<Shellexec>("Shellexec", 1, 0, "Shellexec", [](QQmlEngine*, QJSEngine*) -> QObject* { return new Shellexec; });
    qmlRegisterType<Shellexec>("Shellexec", 1, 0, "Shellexec");
}
