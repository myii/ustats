#include <QDebug>
#include <QTextStream>
#include <QProcess>
#include "shellexec.h"

Shellexec::Shellexec(QObject *parent) :
    QObject(parent),
    m_process(new QProcess(this))
{
}

QString Shellexec::launch(const QString program)
{
    QByteArray ba;
    QByteArray bytes;
    QString output;
    m_process->start(program);
    m_process->waitForFinished(-1);
    qDebug() << "Shellexec launch error:" << m_process->error();
    // if (m_process->waitForStarted(-1)) {
    //   while(m_process->waitForReadyRead(-1)) {
    //     ba += m_process->readAllStandardOutput();
    //     output += QString::fromLocal8Bit(ba);
    //   }
    // }
    //output = QString::fromLocal8Bit(ba);
    output = m_process->readAllStandardOutput();

    qDebug()<< "return value: " << output;
    return output;
}
