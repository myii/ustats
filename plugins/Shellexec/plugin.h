#ifndef SHELLEXECPLUGIN_H
#define SHELLEXECPLUGIN_H

#include <QQmlExtensionPlugin>

class ShellexecPlugin : public QQmlExtensionPlugin {
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QQmlExtensionInterface")

public:
    void registerTypes(const char *uri);
};

#endif
